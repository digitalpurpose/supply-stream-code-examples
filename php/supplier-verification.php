<?php

//-----------------------------------------------------------------------------
// Helper functions

function authenticate($client_id, $client_secret, $username, $password)
{
    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, "https://supplynation--staging.my.salesforce.com/services/oauth2/token");
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE); // stops the response from being printed to screen

    $data = [
        'grant_type' => 'password',
        'client_id' => $client_id,
        'client_secret' => $client_secret,
        'username' => $username,
        'password' => $password,
    ];
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

    $result = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($result, true);

    if (!empty($response['error'])) {
        echo "Invalid login details (error: " . $response['error'] . ")\n";
        die;
    }

    $access_token = $response['access_token'];
    return $access_token;
}

function verify_suppliers($access_token, $path_to_file)
{
    echo "================================================================================\n";
    echo "Verifying suppliers\n";
    echo "================================================================================\n";

    $curl = curl_init();

    $url = "https://supplystreamapi.supplynation.org.au/api/lite/csv";

    $fields = [
        'file' => new \CurlFile($path_to_file, 'text/csv', 'unverified-suppliers.csv')
    ];

    curl_setopt($curl, CURLOPT_URL, $url);
    $auth = 'Authorization: Bearer ' . $access_token;
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data', $auth, 'Accept: text/csv'));
    curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, FALSE);

    curl_exec($curl);
    curl_close($curl);
}

//-----------------------------------------------------------------------------
// Main logic

$client_id = 'YOUR_CLIENT_ID';
$client_secret = 'YOUR_CLIENT_SECRET';
$username = 'YOUR_USERNAME';
$password = 'YOUR_PASSWORD';
$path_to_file = '/path/to/file.csv';

echo("Connecting to Supply Nation API\n");

$access_token = authenticate($client_id, $client_secret, $username, $password);
echo("Access token: " . $access_token . "\n");

verify_suppliers($access_token, $path_to_file);

echo "================================================================================\n";
echo("Job done\n");
echo "================================================================================\n";


?>
