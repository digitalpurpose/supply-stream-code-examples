<?php

//-----------------------------------------------------------------------------
// Helper functions

function authenticate($url, $client_id, $client_secret, $username, $password)
{
    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, $url . "/oauth2/token");
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE); // stops the response from being printed to screen

    $data = [
        'grant_type' => 'password',
        'client_id' => $client_id,
        'client_secret' => $client_secret,
        'username' => $username,
        'password' => $password,
    ];
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

    $result = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($result, true);

    if (!empty($response['error'])) {
        echo "Invalid login details (error: " . $response['error'] . ")\n";
        die;
    }


    $access_token = $response['access_token'];
    return $access_token;
}

function get_suppliers($url, $access_token, $next = null)
{
    $curl = curl_init();

    $url = $url . "/apexrest/impact/public/v3/supplier";

    $data = [
        'next' => $next,
    ];
    $url = sprintf("%s?%s", $url, http_build_query($data));

    curl_setopt($curl, CURLOPT_URL, $url);
    $auth = 'Authorization: Bearer ' . $access_token;
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $auth));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE); // stops the response from being printed to screen

    $result = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($result, true);
    return $response;
}

//-----------------------------------------------------------------------------
// Main logic

$url = 'https://supplynation--staging.my.salesforce.com/services';
$client_id = 'YOUR_CLIENT_ID';
$client_secret = 'YOUR_CLIENT_SECRET';
$username = 'YOUR_USERNAME';
$password = 'YOUR_PASSWORD';


echo("Connecting to Supply Nation API\n");

$access_token = authenticate($url, $client_id, $client_secret, $username, $password);
echo("Access token: " . $access_token . "\n");

$suppliers = get_suppliers($url, $access_token);

$page = 1;
$next = null;
do {
    $suppliers = get_suppliers($url, $access_token, $next);
    if ($page == 1) {
        echo "================================================================================\n";
        echo "Found " . $suppliers['total'] . " total suppliers\n";
        echo "================================================================================\n";
    }
    echo "Found " . count($suppliers['items']) . " suppliers for page " . $page . "\n";

    foreach ($suppliers['items'] as $supplier) {
        echo "  - " . $supplier['accountName'] . "\n";
    }

    $next = $suppliers['next'];
    $page++;
} while (!empty($next));

echo "================================================================================\n";
echo("Job done\n");
echo "================================================================================\n";


?>
