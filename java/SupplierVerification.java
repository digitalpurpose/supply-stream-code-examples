import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

class SupplierVerification {
    private final static String URL = "https://supplystreamapi.supplynation.org.au/api/lite/csv";
    private final static String ACCESS_TOKEN = "YOUR_ACCESS_TOKEN";
    private final static String INPUT_FILE_PATH = "/path/to/file.csv";

    public static void main(String[] args) throws IOException {
        File file = new File(INPUT_FILE_PATH);

        URL url = new URL(URL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Accept", "text/csv");
        conn.setRequestProperty("Authorization", "Bearer " + ACCESS_TOKEN);

        /* Construct the request body with your preferred method */
        writeCsvToRequestBody(conn, file);

        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("HTTP error code: " + conn.getResponseCode());
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        try {
            /* Handle the response CSV file as required */
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (Exception e) {
            System.out.print("Unable to parse response body");
        }
        conn.disconnect();
    }

    public static void writeCsvToRequestBody(HttpURLConnection connection, File file) throws IOException {
        MultipartHelper helper = new MultipartHelper(connection);
        helper.addFilePart("file", file);
        helper.finish();
    }
}
