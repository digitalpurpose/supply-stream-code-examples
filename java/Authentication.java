import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

class Authentication {
    private final static String URL = "https://supplynation--Staging.my.salesforce.com/services/oauth2/token";
    private final static String CLIENT_ID = "YOUR_CLIENT_ID";
    private final static String CLIENT_SECRET = "YOUR_CLIENT_SECRET";
    private final static String USERNAME = "YOUR_USERNAME";
    private final static String PASSWORD = "YOUR_PASSWORD";

    private static String convertParamsToEncodedFormRequestBody() throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        sb.append("grant_type=password&");
        sb.append("client_id=").append(URLEncoder.encode(CLIENT_ID, "utf-8")).append("&");
        sb.append("client_secret=").append(URLEncoder.encode(CLIENT_SECRET, "utf-8")).append("&");
        sb.append("username=").append(URLEncoder.encode(USERNAME, "utf-8")).append("&");
        sb.append("password=").append(URLEncoder.encode(PASSWORD, "utf-8"));
        return sb.toString();
    }

    private static String convertResponseToString(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static void main(String[] args) throws IOException {
        URL url = new URL(URL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setDoOutput(true);
        try (OutputStream os = conn.getOutputStream()) {
            byte[] input = convertParamsToEncodedFormRequestBody().getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("HTTP error code: " + conn.getResponseCode());
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        try {
            /* Parse the response with your preferred JSON parsing method */
            System.out.print(convertResponseToString(reader));
        } catch (Exception e) {
            System.out.print("Unable to parse response body");
        }
        conn.disconnect();
    }
}
