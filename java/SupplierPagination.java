import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

class SupplierPagination {
    private final static String URL = "https://supplynation--Staging.my.salesforce.com/services/apexrest/impact/public/v3/supplier";
    private final static String ACCESS_TOKEN = "YOUR_ACCESS_TOKEN";

    private static String convertResponseToString(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static void main(String[] args) throws IOException {
        String endpoint = URL;
        String next = null;
        if (next != null) {
            endpoint += "?next=" + next;
        }
        URL url = new URL(endpoint);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Authorization", "Bearer " + ACCESS_TOKEN);
        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("HTTP error code: " + conn.getResponseCode());
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        try {
            /* Parse the response with your preferred JSON parsing method */
            System.out.print(convertResponseToString(reader));
        } catch (Exception e) {
            System.out.print("Unable to parse response body");
        }
        conn.disconnect();
    }
}
